# Japanese translation for ubuntu-calendar-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2019-11-10 18:36+0000\n"
"Last-Translator: M.Sugahara <equaaqua@hotmail.com>\n"
"Language-Team: Japanese <https://translate.ubports.com/projects/ubports/"
"calendar-app/ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "新しいイベント"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "カレンダー"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "戻る"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "同期"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "同期中"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "オンラインカレンダーを追加"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "選択解除できません"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr "読み取り専用のカレンダーが選択されています。イベントを書き込めません"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "完了"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "通知なし"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "0分"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1週間"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1日"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1時間"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1分"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5分"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10分"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "15分"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "30分"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "1時間"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "2時間"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "1日"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "2日"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "1週間"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "2週間"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "カスタム"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "予定表"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "すべてのカレンダーが無効になっています"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "近日中のイベントはありません"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "カレンダーを有効にする"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "イベント名なし"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "場所なし"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "繰り返さない"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "X回後"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "指定した日数経過後"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "新しいイベント"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1件"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1件の終日イベント"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "イベントを編集"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""
"このイベント「%1」だけを編集しますか、一連のすべてのイベントを編集しますか？"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "一連のイベントを編集"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "このイベントを編集"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "キャンセル"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "連絡先がありません"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "連絡先を検索"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "繰り返し指定した予定の削除"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "イベントを削除"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr ""
"このイベント「%1」だけを削除しますか、一連のイベントすべてを削除しますか？"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "イベント「%1」を削除します。よろしいですか？"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "一連のイベントを削除"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "このイベントを削除"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "削除"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"カレンダーアプリは--starttime、--endtime、--newevent、--eventidの4つの引数を"
"指定できます。これはシステムによって管理されています。詳しいことはソースコー"
"ドのコメントを参照してください。"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "日"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "週"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "月"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "年"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "全日"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "設定"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "週番号を表示"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr ""

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr ""

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "デフォルトリマインダー"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "デフォルトカレンダー"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr ""

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "週"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "繰り返し"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "繰り返し："

#: ../qml/EventRepetition.qml:233
#, fuzzy
msgid "Interval of recurrence"
msgstr "X回後"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "繰り返しの終了"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "繰り返し"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "日付"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "今日"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "%1 年"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr "終了時刻を開始時刻より前にできません"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "保存"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "エラー"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "開始"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "終了"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "終日のイベント"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "イベントの詳細"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "イベント名"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "概要"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "場所"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "カレンダー"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "ゲスト"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "ゲストを追加"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "リマインダー"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "M'月'"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "yyyy'年'M'月'"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "第%1週"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "編集"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "出席"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "欠席"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "不明"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "返信なし"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "アカウントを選択してください。"

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "色の選択"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "一度だけ"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "毎日"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "平日"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "%1、%2、%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "%1、%2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "毎週"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "毎月"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "毎年"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr ""

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: ../qml/EventUtils.qml:95
#, fuzzy
msgid "; every %1 weeks"
msgstr "%1週間"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr ""

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "オンラインアカウントと同期するUbuntu用のカレンダー。"

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr ""
"calendar;event;day;week;year;appointment;meeting;カレンダー;イベント;予定表;"
"日;週;年;約束;ミーティング;"

#~ msgid "Show lunar calendar"
#~ msgstr "月齢を表示"
